<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tridev LMS</title>


    <script src="{{ asset('js/app.js') }}" defer></script>
   

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ URL::asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/backend_css/style.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('css/backend_css/custom.css') }}">
    <!-- End layout styles -->
    <!-- <link rel="shortcut icon" href="{{ URL::asset('images/backend_images/favicon.png') }}" /> -->
  </head>
  <body>
<div class="container-scroller">
    @include('partials.header')
    <div class="container-fluid page-body-wrapper">
        @include('partials.sidebar')
        <div class="main-panel">
          <div class="content-wrapper">
            @yield('content')
          </div>
        </div>
    </div>
 </div>
 
    <script src="{{ URL::asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/off-canvas.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/hoverable-collapse.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/misc.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/file-upload.js') }}"></script>
</body>
</html>