@extends('layouts.master')
@section('content')
            <div class="row" id="proBanner">
              <div class="col-12">
                <span class="d-flex align-items-center purchase-popup">
                   <ul>
                        @foreach($errors->all() as $error)
                        <li style="color: red;">{{$error}}</li>
                        @endforeach
                      </ul>
                  <a href="#" class="btn ml-auto download-button Importcsv_button" onclick="openForm()">
                  Import CSV</a>
                  <a href="{{ route('lead.create') }}" class="btn purchase-button">Add Lead</a>
                </span>

                  <div class="form-popup" id="myForm">

                      <form action="{{ route('lead.import') }}" class="form-container" enctype="multipart/form-data" method="POST">
                           @csrf
                       
                        <label for="csv"><h4>Import Csv</h4></label>
                        <input type="file" placeholder="Upload Csv" name="importcsv" required>

                        <button type="submit" class="btn">Import</button>
                        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
                      </form>
                    </div>

                
              </div>
            </div>
            <div class="row">
              <div class="col-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                     
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                 
                   
                    <!-- <p class="card-description"> Add class <code>.table-{color}</code>
                    </p> -->
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-bordered">
                          <thead>
                             
                            <tr>
                              <th>Domain Id</th>
                              <th>Domain Name</th>
                              <th>Country</th>
                              <th>State</th>
                              <th>City</th>
                              <th>Total Emails</th>
                              <th>Status</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($leads as $lead)
                            <tr >
                               <td>{{ $lead->domain_id }} </td>
                               <td><a style="color:blue;" href="{{ route('email.view', $lead->domain_id) }}">{{ $lead->slug }} </a></td>
                               <td>{{ $lead->country }}</td>
                               <td>{{ $lead->state }}</td>
                               <td>{{ $lead->city }}</td>
                               <td>{{ $lead->totalemail }}</td>
                               <td>@php  if ($lead->status == 0){ echo "Inactive";
                               } else { echo "Active";} @endphp</td>
                               <td><a href="{{ route('lead.edit', $lead->domain_id) }}"> Edit</a></td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection
<!-- <script>
  $(document).ready(function(){
  $(".Importcsv_button").click(function(){
  $("#Importcsv_form").toggle();
  });
});
</script>

 -->
 <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
 <script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>