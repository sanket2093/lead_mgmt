@extends('layouts.master')
@section('content')
    <div class="row" id="proBanner">
      <div class="col-12 align-items-left">
        <span class="d-flex align-items-center purchase-popup">
          <a href="{{ route('email.index') }}" class="btn ml-auto download-button">All Email</a>
          <a href="{{ route('email.create') }}" class="btn purchase-button">Add Email</a>
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Email</th>
                          <th>Domain ID</th>
                          <th>Domain Name</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach ($emails as $email)
                        <tr>
                           <td>{{ $email->email_id }} </td>
                           <td>{{ $email->email }}</td>
                           <td>{{ $email->domain_id }}</td>
                           <td>{{ $email->domain_name }}</td>
                           <td>@php  if ($email->status == 0){ echo "Inactive";
                           } else { echo "Active";} @endphp</td>
                           <td><a href="{{ route('email.edit', $email->email_id) }}"> Edit</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
      </div>
    </div>
@endsection
