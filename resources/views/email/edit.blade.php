@extends('layouts.master')

@section('content')
           <!--  <div class="page-header">
              <h3 class="page-title"> Form elements </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Forms</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                </ol>
              </nav>
            </div> -->
            <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
                  <div class="col-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                    <h4 class="card-title">Basic form elements</h4>
                    <p class="card-description"> Basic form elements </p>
                    
                    <form class="forms-sample" action="{{ route('email.update', ['email_id' => $email->email_id]) }}" method="POST">
                       @csrf 
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$email->email }}">
                      </div>
                      <div class="form-group">
                        <label for="status">Status :</label>
                        <input type="radio" name="status" value="1"<?php if ($email->status == 1) { ?> checked="checked"  <?php } ?>>Active
                        <input type="radio" name="status" value="0" <?php if ($email->status == 0) { ?> checked="checked"  <?php } ?>>Inactive
                      </div>
                      <!-- <div class="form-group">
                        <label>File upload</label>
                        <input type="file" name="img[]" class="file-upload-default">
                        <div class="input-group col-xs-12">
                          <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                          <span class="input-group-append">
                            <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                          </span>
                        </div>
                      </div> -->
                      <!-- <div class="form-group">
                        <label for="exampleInputCity1">City</label>
                        <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
                      </div>
                      <div class="form-group">
                        <label for="exampleTextarea1">Textarea</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                      </div> -->
                      <button type="submit" class="btn btn-gradient-primary mr-2">Update</button>
                      <button class="btn btn-light">Cancel</button>
                     
                    </form>
                   
                        </div>
                      </div>
                  </div>
              </div>
          <!-- partial -->
        </div>
   @endsection
