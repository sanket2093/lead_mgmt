    <script src="{{ URL::asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/off-canvas.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/hoverable-collapse.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/misc.js') }}"></script>
    <script src="{{ URL::asset('js/backend_js/file-upload.js') }}"></script>
</body>
</html>