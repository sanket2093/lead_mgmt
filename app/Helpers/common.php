<?php 
namespace App\Helpers;

class EmailHelper
{
	public function storeemail()
	{


        $request['slug'] = $this->get_domain($request['email']);
        $rules= [
            'email' => 'required | unique:email',
            
        ];

        $customMessages = [
            'email.required' => 'Please Enter Email',
            'email.unique' => 'Sorry, This Email Is Already Used By Another User. Please Try With Different One, Thank You.'
            
        ];
        
        $this->validate($request, $rules,$customMessages);
        
        $domainName  = $this->get_domain($request['email']);
        $leadsData = Lead::where('domain_name', '=', $domainName)->first();
        if(empty($leadsData)) {
          $objLead = new Lead();
          $objLead->domain_name = $domainName;
          $objLead->slug = $domainName;
          $objLead->country = $request['country'];
          $objLead->state = $request['states'];
          $objLead->city = $request['city'];
          $objLead->save();
          $domainId = $objLead->domain_id;
        } else {
          $domainId = $leadsData->domain_id;
        }
        $objemail= new Email();
        $objemail->email = $request['email'];
        $objemail->domain_id = $domainId; 
        $objemail->domain_name = $domainName;
        $objemail->status = $request['status'];
        $objemail->save();
        return redirect('email');
	}
}

?>