<?php 
 namespace App\Models;
 use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\DB;

 class Email extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'email';
    protected $primaryKey = 'email_id';
    protected $fillable = [
        'email_id','domain_name','domain_id','email','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email_id', 'remember_token',
    ];

    public function listallemails()
    {
       $emailall = DB::select('select * from email');
        return $emailall;
        
    }
    public function ListEmail($id)
    {

        $email = DB::table('email')
               ->join('lead','lead.domain_id', '=','email.domain_id')
               ->select('email.*','lead.domain_id','lead.domain_name')
               ->where('email.domain_id','=',$id);
        $email = $email->get(); 
        return $email;
    }


}


?>