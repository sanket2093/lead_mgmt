<?php 
 namespace App\Models;
 use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\DB;

 class Lead extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lead';
    protected $primaryKey = 'domain_id';
    protected $fillable = [
        'domain_id','domain_name','country','state','city','status','slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'domain_id', 'remember_token',
    ];

    public function ListLeads()
    {
        $leads = DB::select('select * from lead');
        return $leads; 
    }
    public function ListLeadwithid()
    {

        
        $Lead = DB::table('lead')
               ->leftjoin('email','lead.domain_id', '=','email.domain_id')
               ->select('lead.*',DB::raw('COUNT(email.email) AS totalemail'))
               ->groupBy('lead.domain_id');
        $Lead = $Lead->get();
        return $Lead;
        
    }

}


?>