@php
        $domainName  = $this->get_domain($request['email']);
        $leadsData = Lead::where('domain_name', '=', $domainName)->first();
        if(empty($leadsData)) {
          $objLead = new Lead();
          $objLead->domain_name = $domainName;
          $objLead->slug = $domainName;
          $objLead->country = $request['country'];
          $objLead->state = $request['states'];
          $objLead->city = $request['city'];
          $objLead->save();
          $domainId = $objLead->domain_id;
        } else {
          $domainId = $leadsData->domain_id;
        }
        $objemail= new Email();
        $objemail->email = $request['email'];
        $objemail->domain_id = $domainId; 
        $objemail->domain_name = $domainName;
        $objemail->status = $request['status'];
        $objemail->save();
        return redirect('email');
    }
@endphp