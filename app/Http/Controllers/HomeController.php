<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Lead;
use App\Models\Email;
use App\User;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $objLead = new Lead(); 
        $arrDataLead = $objLead->ListLeads();
        $leadcount = count($arrDataLead);
        

        $objEmail = new Email();
        $arrDataEmail = $objEmail->listallemails();
        $Emailcount = count($arrDataEmail);

        $objUsers = new User();
        $arrDataUser = $objUsers->allusers();
        $Usercount = count($arrDataUser);

        return view('home',compact('leadcount',$leadcount,'Emailcount',$Emailcount,'Usercount',$Usercount));

    }
    public function ListLeads(Request $request)
    {
        $objLead = new Lead(); 
        $arrData = $objLead->ListLeads();
        $leadcount = count($arrData);

        return view('lead/list',compact('leadcount',$leadcount))->with('leads', $arrData);

    }

    /*public function Userlisting(Request $request)
    {
        $ObjUsers = new User();
        $arrData= $ObjUsers->Userlisting();
        print_r($arrData);exit();
        return view('home')->with('users', $arrData);

    }*/

    public function create(Request $request)
    {
        return view('lead/create');
    }
     public function store(Request $request)
    {
        
        $objLead= new Lead();
        $objLead->domain_name = $request['domain_name'];
       
        $objLead->country = $request['country'];
        $objLead->state = $request['states'];
        $objLead->city = $request['city'];
 /*       $objLead->total_emails = $request['Total_emails'];*/
        $objLead->status = $request['status'];

        $objLead->save();

      return redirect('home');
    }
   /* public function edit($domain_id)
    {
        return view('lead/edit');
    }*/

    
}
