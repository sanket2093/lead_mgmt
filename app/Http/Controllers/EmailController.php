<?php

namespace App\Http\Controllers;
namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Email;
use App\Models\Lead;
use Illuminate\Support\Facades\Validator;


class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function get_domain($url)
    {
      $pieces = parse_url($url);
      $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }else{
        return false;
      }
    }
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $objemail = new Email(); 
        //$arrData = $objemail->ListEmail();
        $arrData = $objemail->listallemails();

         return view('email/index')->with('emails', $arrData);
    }

    public function view($id){
      $objemail = new Email(); 
        $arrData = $objemail->ListEmail($id);
        //$arrData = $objemail->listallemails($id);
        return view('email/index')->with('emails', $arrData);
    }

    public function create(Request $request)
    {
        //return view('email/create',compact('id'));
        return view('email/create');
    }

     public function store(Request $request, $id)
    {  
      \App\Helpers\common::instance()->storeemail();
    }

    public function edit($email_id)
    {
        $email = email::where('email_id', $email_id)->first();

        return view('email/edit', compact('email'));
    }
    public function update($email_id,Request $request)
    {
        $objemail=Email::find($email_id);
        $objemail->email = $request['email'];
 /*       $objLead->total_email = $request['Total_email'];*/
        $objemail->status = $request['status'];
        $objemail->save();
        return redirect('email');
    }

    
}
