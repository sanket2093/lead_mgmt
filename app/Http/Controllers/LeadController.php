<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Lead;
use App\Models\Email;
use Illuminate\Support\Facades\Validator;



class LeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* function is_valid_domain_name($domain_name)
    {
    return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
            && preg_match("/^.{1,253}$/", $domain_name) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
    }*/

    function get_domain($url)
    {
      $pieces = parse_url($url);
      $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }else{
        return false;
      }
    }
    public function index()
    {
        $objLead = new Lead(); 
        //$arrData = $objLead->ListLeads();
        $arrData = $objLead->ListLeadwithid();
        return view('lead/index')->with('leads', $arrData);
    }
    

    public function create(Request $request)
    {
        return view('lead/create');
    }
     public function store(Request $request)
    {

        $request['slug'] = $this->get_domain($request['domain_name']);
        $rules= [
            'domain_name' => 'required',
            'slug' =>'unique:lead'
        ];

        $customMessages = [
            'domain_name.required' => 'Please Enter Domain Name',
            'slug.unique' => 'Sorry, This Domain Is Already Used By Another User. Please Try With Different One, Thank You.'
        ];
        
        $this->validate($request, $rules,$customMessages);
        

        $objLead= new Lead();
       
        $objLead->domain_name = $request['domain_name'];
       
        $objLead->country = $request['country'];
        $objLead->state = $request['states'];
        $objLead->city = $request['city'];
 /*       $objLead->total_emails = $request['Total_emails'];*/
        $objLead->status = $request['status'];
        //$query = "SELECT slug FROM lead WHERE slug='".$this->get_domain($request['domain_name'])."'";
        $query = DB::table('lead')->where('slug', $this->get_domain($request['domain_name']))->first();
        
        if (!empty($query)) {

        }else{
            $objLead->slug = $this->get_domain($request['domain_name']);
            $objLead->save();
            
        }
       
      return redirect('lead');
    }
    public function edit($domain_id)
    {
        $lead = lead::where('domain_id', $domain_id)->first();

        return view('lead/edit', compact('lead'));
    }
    public function update($domain_id,Request $request)
    {
        $objLead=Lead::find($domain_id);
        $objLead->domain_name = $request['domain_name'];
       
        $objLead->country = $request['country'];
        $objLead->state = $request['states'];
        $objLead->city = $request['city'];
 /*       $objLead->total_emails = $request['Total_emails'];*/
        $objLead->status = $request['status'];
        $objLead->save();
        return redirect('lead');
    }
    public function import(request $request)
    {

       $data = $request['importcsv'];
       $rules= [
            'importcsv' => 'required|mimes:csv,txt,xls,xlsx'
            
        ];

        $customMessages = [
            'importcsv.mimes' => 'Please upload only csv or excel file'
            
        ];
        
        $this->validate($request, $rules,$customMessages);


        $objLead= new Lead();
        $path = $request->file('importcsv')->getRealpath();
        $file = fopen($request['importcsv'],"r");
        //$data = fgetcsv($file);
        
        while(!feof($file))
        {
            $data =fgetcsv($file);
            if($data[0]){
                echo $data[0]." ----- ";
                $inserted_data = array('email'=>$data[0]);
                Email::create($inserted_data);
            }
        }

        exit();
        // dd($rowdata);
        // foreach ($rowdata as $key => $value) {
            
        //     $inserted_data = array(
        //                             'email'=>$value[0],
        //                           );
        //     //Lead::create($inserted_data);
        // }
        //$data = implode('',$rowdata)
       // dd($rowdata);

    }

    
}
