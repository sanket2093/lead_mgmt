<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (!Auth::check()) {
 Route::get('/', function () {
    return view('auth/login');
});
}

  


Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');
Route::get('home/create', 'HomeController@create')->name('home.create');
Route::post('home/create', 'HomeController@store')->name('home.store');*/


Route::any('home', 'homecontroller@index')->name('home.index');//index and search
//Route::resource('/lead', 'LeadController');
Route::any('lead', 'LeadController@index')->name('lead.index');//index and search
Route::get('lead/create', 'LeadController@create')->name('lead.create');//call create form
Route::post('lead/store', 'LeadController@store')->name('lead.store');//save create
Route::get('lead/edit/{id}', 'LeadController@edit')->name('lead.edit');//call form edit
Route::post('lead/update/{id}', 'LeadController@update')->name('lead.update');//save edit
Route::delete('lead/destroy/{id}', 'LeadController@destroy')->name('lead.destroy');//delete lead
//Route::get('lead/importcsv', 'LeadController@importcsv')->name('lead.importcsv');//call create form
Route::post('lead/import', 'LeadController@import')->name('lead.import');//save create


Route::any('email', 'EmailController@index')->name('email.index');//index and search
Route::get('email/view/{id}', 'EmailController@view')->name('email.view');//index and search
Route::get('email/create', 'EmailController@create')->name('email.create');//call create form
//Route::get('email/create/{id}', 'EmailController@create')->name('email.create');//call create form
Route::post('email/store/{id}', 'EmailController@store')->name('email.store');//save create
Route::get('email/edit/{id}', 'EmailController@edit')->name('email.edit');//call form edit
Route::post('email/update/{id}', 'EmailController@update')->name('email.update');//save edit
Route::delete('email/destroy/{id}', 'EmailController@destroy')->name('email.destroy');//delete lead

